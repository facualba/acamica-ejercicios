# Ejercicios con recursión

- Crear una function llamada `contarHastaDiez`, que cuente, recursivamente, desde 1 hasta 10.

- Crear una function llamada `sumar`, que sume, recursivamente, desde 1 hasta n.

- Crear una function llamada `factorial`, que calcule, recursivamente, el factorial de n.

***

# Ejercicios con funciones callback

- Crear una function llamada `invoke`, que reciba como parametro una `function callback` y muestre por consola `Hola mundo, soy un callback`

- Crear una function llamada `regresiva`, que reciba como parametro un numero `n` y haga una cuenta regresiva.

- Imprimir por consola algún mensaje de prueba `luego de n segundos`.

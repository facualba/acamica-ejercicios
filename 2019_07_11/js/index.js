function contarHastaDiez(n) {
  if (n <= 0 || n > 10) return;
  console.log(n);
  n++;
  return contarHastaDiez(n);
}

function sumar(n) {
  if (n <= 0) return 0;
  return sumar(n - 1) + n;
}

function factorial(n) {
  if (n < 0) return;
  if (n === 0) return 1;
  return n * factorial(n - 1);
}

// var num = parseInt(prompt("Número: "));
// contarHastaDiez(num);

function invoke(fn) {
  fn();
}

function regresiva(n) {
  if (n <= 0) {
    return;
  }
  console.log(n);
  n--;
  return regresiva(n);
}

regresiva(10);

setTimeout(function () {
  console.log("Un mensaje de prueba");
}, 2000);


// invoke(function () {
//   console.log("Hola mundo, soy un callback");
// });

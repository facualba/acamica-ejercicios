(function () {

  function Alumno(nombre, apellido, edad, direccion, dni, promedio) {

    // if (typeof objeto != 'object') {
    //   console.error("Algo salio mal!!");
    // }
    //
    // this.datos = objeto;

    this.nombre = nombre;
    this.apellido = apellido;
    this.edad = edad;
    this.direccion = direccion;
    this.dni = dni;
    this.promedio = promedio;

    // TODO: crear getters y setters para este objeto
  }

  function Punto(x, y) {

    // if (typeof objeto != 'object') {
    //   console.log("ups!");
    // }

    this.x = x;
    this.y = y;

    // TODO: crear getters y setters para este objeto
  }

  // TODO: llamadas al objeto y definicion
  // objeto literal
  // var datos = {
  //   nombre: 'Facu',
  //   apellido: 'Alba',
  //   edad: 26,
  //   direccion: {
  //     calle: 'Calle falsa',
  //     altura: 123
  //   },
  //   dni: 12345678,
  //   promedio: 10
  // };

  var alumno = new Alumno('Facu', 'Alba', 26, {calle: 'calle falsa', altura: 123}, 12345678, 10);
  console.log(alumno);
  var punto = new Punto(20, 10);
  console.log(punto);
})();

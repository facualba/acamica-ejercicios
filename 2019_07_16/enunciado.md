# Ejercicios con objetos

- Crear un objeto que represente a un alumno, debe recibir como parámetro un objeto alumnos y mostrar en consola todos los datos, nombre, apellido, edad, dni, dirección y promedio.
- Crear getters y setters para el objeto alumno

- Crear un objeto que represente a un Punto, debe recibir como parámetro un objeto punto y mostrar en consola todos los datos, x e y.
- Crear getters y setters para el objeto alumno

```js
// Objeto ejemplo:

var estudiante = {
  nombre: 'Facu',
  apellido: 'Alba',
  dni: 12345678,
  direccion: {
    calle: 'Calle falsa',
    numero: 123
  },
  promedio: 10
}

var punto = {
  x: 10,
  y: 5
}

```

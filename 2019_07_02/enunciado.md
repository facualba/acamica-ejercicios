# Javascript conversión de tipos

- Crear una variable y asignarle un valor de tipo `String`
- Convertir, la variable que creamos anteriormente, a tipo `Number`

- Crear una variable y asignarle un valor de tipo `Number`
- Convertir, la variable que creamos anteriormente, a tipo `String`

- Crear una variable y asignarle un valor de tipo `Number decimal`
- Convertir, la variable que creamos anteriormente, a tipo `Number integer`
***
# Más ejercicios !!

- Hacer un programa que pida al usuario que ingrese un número entre 1 y 10, y en caso de que sea igual a 7, muestre un `"ADIVINASTE"`, y en caso de que el número ingresado sea distinto de 7, muestre `"PERDISTE"`

- Hacer un programa que pida al usuario que ingrese un número, en caso de que sea igual a numeroRandom, muestre un `"LE PEGASTE"`, y en caso de que el número ingresado sea distinto de numeroRandom, muestre `"LE ERRASTE"`. Usar `Math.floor(Math.random() * Math.floor(99)) + 1` para generar numeros aleatorios

- Crear una `function` que reciba 2 parámetros, `precio e iva`, y devuelva el precio con iva incluido. Si no recibe el iva, `aplicará el 21 por ciento por defecto`.

- Crear una `function` que eleve un número `x` a un número `e`.

- Crear una `function` que calcule la raíz cuadrada de un número `x`.

- Crear una `function` que reciba un `String` como parámetro, y lo devuelva en mayusculas.

- Crear una `function` que reciba un `String` como parámetro, y lo devuelva en minusculas.

- Crear una `function` que devuelva la fecha actual.

- Crear una `function` que tome dos parametros, `texto` y `palabra`, que nos indique si la `palabra` está en el `texto`.

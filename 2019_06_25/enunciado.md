# Crear un programa que realice lo siguiente

- Pedir nombre mediante `prompt` y mostrarlo en consola junto con algun texto de saludo. Ej: `Hola Pedro!`
- Pedir un numero mediante `prompt`, parsearlo, sumarlo a otro que este en una variable y luego mostrar el resultado en consola.
- Pedir un numero mediante `prompt`, parsearlo, restarlo a otro que este en una variable y luego mostrar el resultado en alerta.
- Pedir un numero mediante `prompt`, luego otro, parsearlos y sumarlos, luego mostrar el resultado en consola.

# Crear una función que muestre el tipo de día en consola

- La función tiene que recibir como parámetro un día de la semana y mostrar en consola `Es día hábil`, `Es día de fin de semana` o `No es un día valido` según corresponda.
- Pedirle al usuario que ingrese un día tres veces y llamar a la función cada vez para mostrar el resultado en consola.

# Crear un programa que muestre si un día es fin de semana según dato ingresado por el usuario

- Pedir mediante `prompt` al usuario que ingrese un día de la semana y mostrar en consola un mensaje que nos indique si el día es fin de semana o no.

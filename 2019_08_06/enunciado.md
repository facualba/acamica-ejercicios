# Ejercicios con arreglos

- Crear una función que calcule el promedio de un array.
```js
// Array ejemplo:

var examResults = [ 7, 5, 6, 4, 3, 2, 8 ]

```

- Crear una función que añada un numero N al final de un array.
```js
// Ejemplo n = 1999
var arr = [ 1, 2, 3, 5, 1999 ]

```

- Crear una función que añada un numero N al principio de un array.
```js
// Ejemplo n = -1999
var arr = [ -1999, 1, 2, 3, 5 ]

```

- Crear una función que sume los primeros N números impares

```js

var arr = [ 1, 2, 3, 5, 6, 7, 8, 9, 10 ]

// resultado = 25

```

- Crear una función que devuelva los numeros pares de una lista dada

```js

var arr = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]

// resultado = [2, 4, 6, 8, 10]

```

- Crear una función que devuelva la posición del numero en una lista

```js
// Ejemplo n = 10
var arr = [ 50, 10, 20, 14, 7 ]

// resultado = posición 1

```

- Crear una función que dada una posición actualize el valor del elemento en dicha posición

```js
// Ejemplo pos = 3, n = 50
var arr = [ 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 ]

// resultado = [ 1, 2, 3, 50, 5, 6, 7, 8, 9, 10 ]

```

- Crear una función que devuelva un array con numeros pares

```js

// resultado = [0, 2, 4, 6, 8]

```
